# -*- coding: utf-8 -*-
from django.db import models
from solo.models import SingletonModel

from django.conf import settings

CHOICES = [[app.replace("apps.", ''), app.replace("apps.", ''),] for app in settings.MY_APPS]
EFFECT_CHOICES = [
    ['none', 'Не использовать'],
    ['flash', 'flash'],
    ['pulse', 'pulse'],
    ['rubberBand', 'rubberBand'],
    ['shake', 'shake'],
    ['swing', 'swing'],
    ['tada', 'tada'],
    ['wobble', 'wobble'],
    ['bounceIn', 'bounceIn'],
    ['bounceInDown', 'bounceInDown'],
    ['bounceInLeft', 'bounceInLeft'],
    ['bounceInRight', 'bounceInRight'],
    ['bounceInUp', 'bounceInUp'],
    ['bounceOut', 'bounceOut'],
    ['bounceOutDown', 'bounceOutDown'],
    ['bounceOutLeft', 'bounceOutLeft'],
    ['bounceOutRight', 'bounceOutRight'],
    ['bounceOutUp', 'bounceOutUp'],
    ['fadeIn', 'fadeIn'],
    ['fadeInDown', 'fadeInDown'],
    ['fadeInDownBig', 'fadeInDownBig'],
    ['fadeInLeft', 'fadeInLeft'],
    ['fadeInLeftBig', 'fadeInLeftBig'],
    ['fadeInRight', 'fadeInRight'],
    ['fadeInRightBig', 'fadeInRightBig'],
    ['fadeInUp', 'fadeInUp'],
    ['fadeInUpBig', 'fadeInUpBig'],
    ['fadeOut', 'fadeOut'],
    ['fadeOutDown', 'fadeOutDown'],
    ['fadeOutDownBig', 'fadeOutDownBig'],
    ['fadeOutLeft', 'fadeOutLeft'],
    ['fadeOutLeftBig', 'fadeOutLeftBig'],
    ['fadeOutRight', 'fadeOutRight'],
    ['fadeOutRightBig', 'fadeOutRightBig'],
    ['fadeOutUp', 'fadeOutUp'],
    ['fadeOutUpBig', 'fadeOutUpBig'],
    ['flipInX', 'flipInX'],
    ['flipInY', 'flipInY'],
    ['flipOutX', 'flipOutX'],
    ['flipOutY', 'flipOutY'],
    ['lightSpeedIn', 'lightSpeedIn'],
    ['lightSpeedOut', 'lightSpeedOut'],
    ['rotateIn', 'rotateIn'],
    ['rotateInDownLeft', 'rotateInDownLeft'],
    ['rotateInDownRight', 'rotateInDownRight'],
    ['rotateInUpLeft', 'rotateInUpLeft'],
    ['rotateInUpRight', 'rotateInUpRight'],
    ['rotateOut', 'rotateOut'],
    ['rotateOutDownLeft', 'rotateOutDownLeft'],
    ['rotateOutDownRight', 'rotateOutDownRight'],
    ['rotateOutUpLeft', 'rotateOutUpLeft'],
    ['rotateOutUpRight', 'rotateOutUpRight'],
    ['hinge', 'hinge'],
    ['rollIn', 'rollIn'],
    ['rollOut', 'rollOut'],
    ['zoomIn', 'zoomIn'],
    ['zoomInDown', 'zoomInDown'],
    ['zoomInLeft', 'zoomInLeft'],
    ['zoomInRight', 'zoomInRight'],
    ['zoomInUp', 'zoomInUp'],
    ['zoomOut', 'zoomOut'],
    ['zoomOutDown', 'zoomOutDown'],
    ['zoomOutLeft', 'zoomOutLeft'],
    ['zoomOutRight', 'zoomOutRight'],
    ['zoomOutUp', 'zoomOutUp'],
    ['slideInDown', 'slideInDown'],
    ['slideInLeft', 'slideInLeft'],
    ['slideInRight', 'slideInRight'],
    ['slideInUp', 'slideInUp'],
    ['slideOutDown', 'slideOutDown'],
    ['slideOutLeft', 'slideOutLeft'],
    ['slideOutRight', 'slideOutRight'],
    ['slideOutUp', 'slideOutUp'],
]

class Section(SingletonModel):
    name = models.CharField(max_length=255, default='Секция с блоками сайта', verbose_name= 'Название секции с блоками')
    scroll = models.BooleanField(default=True, verbose_name='Использовать нестандартную полосу прокрутки')

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Блоки'
        verbose_name = u'Блок'

class Block(models.Model):
    name = models.CharField(max_length=255, blank=True, verbose_name= 'Название блока')
    Section = models.ForeignKey('Section', verbose_name= 'Секция')
    app_name = models.CharField(choices=CHOICES, default='true', max_length=255, verbose_name= 'Название monocle-приложения', help_text='Пример: monocle_main')
    text = models.TextField(verbose_name='Описание', blank=True)
    effect = models.CharField(choices=EFFECT_CHOICES, default='none', max_length=255, verbose_name= 'Эффект появления блока', help_text='Выберите нужный эффект')
    effect_duration = models.FloatField(default=1, verbose_name='Продолж. анимации (сек)')
    effect_delay = models.FloatField(default=0.2, verbose_name='Отсрочка анимации (сек)')
    includeCss = models.BooleanField(default=True, verbose_name='Дополн. CSS')
    includeJs = models.BooleanField(default=True, verbose_name='Дополн.JavaScript')
    position = models.SmallIntegerField(default=0)
    isShown = models.BooleanField(default=True, verbose_name='Отображать')

    def return_effect_duration(self):
        return str(self.effect_duration).replace(',', '.').replace('-', '')

    def return_effect_delay(self):
        return str(self.effect_delay).replace(',', '.').replace('-', '')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = u'Блоки'
        verbose_name = u'Блок'
        ordering = ['position']

class Contacts(SingletonModel):
     phone = models.CharField(max_length=100, blank=True, verbose_name='Телефон', help_text='Введите номер телефона')
     email = models.EmailField(verbose_name='Email для приема заказов', help_text='Введите адрес электронной почты')
     address = models.TextField(verbose_name='Адрес', help_text='Введите адрес')

     class Meta:
         verbose_name = u'Контакты'
         verbose_name_plural = u'Контакты'


from django.apps import AppConfig
class CustomAppConfig(AppConfig):
    name = 'apps.main'
    verbose_name = ''