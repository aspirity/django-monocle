from django.shortcuts import render
from apps.main.models import Section

{% for app in MODULE_LIST %}from apps.{{ app.appname }}.models import {%for model in app.models %}{{model}}{% if not loop.last %},{% endif %}{% endfor %}
{% endfor %}

def home(request):


    context = {
        'monocle_main': Section.objects.all(),
        {% for app in MODULE_LIST %}
        {{app.context_callback}},
        {% endfor %}
    }

    return render(request,'base.html', context)
