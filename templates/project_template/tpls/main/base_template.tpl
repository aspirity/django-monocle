<!DOCTYPE html>
<html>
<head>
    <title>Одностраничный сайт</title>
    <meta charset="UTF-8">
    <meta id="viewport" name="viewport"  content="initial-scale=1.0"/>

    <link rel="icon" type="image/png" href="static/imgs/favicon.png" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="static/assets/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" href="static/assets/owl-carousel/owl.theme.css"/>

    <link rel="stylesheet" href="static/css/mystyle.css"/>
    <script type="text/javascript" src="static/assets/owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="static/assets/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="static/assets/validate/jquery.validate_ru.js"></script>

    <link rel="stylesheet" href="static/assets/wow/animate.css"/>
    <script type="text/javascript" src="static/assets/wow/wow.min.js"></script>

    <script type="text/javascript" src="static/assets/nicescroll/jquery.nicescroll.min.js"></script>

    {% raw %}
        {% for section in monocle_main %}
            {% for sort_app in section.block_set.all%}
                {% if sort_app.isShown == True %}
                    {% if sort_app.includeCss == True %}
                        <link rel="stylesheet" href="static/{{ sort_app.app_name }}/{{ sort_app.app_name }}.css"/>
                    {% endif %}
                {% endif %}
            {% endfor %}
        {% endfor %}
    {% endraw %}

</head>
    <body>

    {% raw %}
         {% for section in monocle_main %}
            {% for sort_app in section.block_set.all%}
                {% if sort_app.isShown == True %}
                    {% with app_path=sort_app.app_name|add:"/"|add:sort_app.app_name|add:".html" %}
                        {% if sort_app.effect != 'none' %}
                        <section class="wow {{ sort_app.effect }}" data-wow-duration="{{ sort_app.return_effect_duration }}s" data-wow-delay="{{ sort_app.return_effect_delay }}s">
                        {% else %}
                        <section>
                        {% endif %}

                        {% include app_path %}
                        </section>
                    {% endwith %}

                    {% if sort_app.includeJs == True %}
                        <script type="text/javascript" src="static/{{ sort_app.app_name }}/{{ sort_app.app_name }}.js"></script>
                    {% endif %}
                {% endif %}
            {% endfor %}

            {% if section.scroll == True %}
                <script>
                    $(document).ready(function() {
                        $("html").niceScroll();
                    });
                </script>
            {% endif %}
        {% endfor %}
    {% endraw %}

    <script>
        new WOW().init();
    </script>

    </body>
</html>