from django.contrib import admin
from .models import *
from grappelli.forms import GrappelliSortableHiddenMixin

from django_summernote.admin import SummernoteModelAdmin
from django_summernote.admin import SummernoteInlineModelAdmin

class BlockInline(GrappelliSortableHiddenMixin, admin.TabularInline, SummernoteInlineModelAdmin):
    model = Block
    sortable_field_name = "position"
    extra = 0

    fieldsets = [
        ('', {'fields': ['app_name', 'name', 'effect', 'effect_duration', 'effect_delay', 'isShown', 'includeCss', 'includeJs', 'position', ], 'classes': ['collapse']}),
    ]
    list_display = ('name', 'app_name', 'text', 'position', 'isShown',)

class SectionAdmin(SummernoteModelAdmin):
    list_display = ('name',)
    list_editable = ('name',)
    inlines = [BlockInline]

class ContactsAdmin(SummernoteModelAdmin):
    fieldsets = [
        ('Контактная информация', {'fields': ['phone', 'email', 'address'], 'classes': ['collapse']}),
    ]
    list_display = ('phone', 'email', 'address')
    list_editable = ('phone', 'email', 'address')

admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Section, SectionAdmin)