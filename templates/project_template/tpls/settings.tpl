# -*- coding: utf-8 -*-

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'hwqk%v$n-la*iskg2ed0y*9tubd-&8rdkhs14fdkwv=f32g&0='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True
THUMBNAIL_DEBUG = True

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    'django.contrib.messages.context_processors.messages',
    "django.core.context_processors.media",
   # 'sekizai.context_processors.sekizai',
)

ALLOWED_HOSTS = []

# Application definition
DEFAULT_APPS = [
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django_summernote',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
   # 'sekizai',
    'apps.main',
    {% for req in MODULE_LIST_REQS %}
    '{{req}}',
    {% endfor %}

]
MY_APPS = [
    {% for app in MODULE_LIST   %}
        'apps.{{app.appname}}',
    {% endfor %}

]

INSTALLED_APPS = DEFAULT_APPS + MY_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = '{{PROJECT_NAME}}.urls'

WSGI_APPLICATION = '{{PROJECT_NAME}}.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Asia/Krasnoyarsk'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = 'public/static'
MEDIA_ROOT = 'public/media'

GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
GRAPPELLI_ADMIN_TITLE = u'Управление сайтом'


# Visual Editor settings
SUMMERNOTE_CONFIG = {
    # Using SummernoteWidget - iframe mode
    'iframe': True,  # or set False to use SummernoteInplaceWidget - no iframe mode

    # Using Summernote Air-mode
    'airMode': True,

    # Use native HTML tags (`<b>`, `<i>`, ...) instead of style attributes (Firefox, Chrome only)
    'styleWithTags': True,

    # Set text direction : 'left to right' is default.
    'direction': 'ltr',

    # Change editor size
    'width': '800px',
    'height': '200',
    'lang': 'ru-RU',

    # --- uncomment what you need ---

    # Customize toolbar buttons
    '''
    'toolbar': [
        ['style', ['style']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'height']],
        ['insert', ['link']],
    ],

    '''

    # Need authentication while uploading attachments.
    '''
    'attachment_require_authentication': True,
    '''

    # Set external media files for SummernoteInplaceWidget.

    'inplacewidget_external_css': (
        '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css',
        '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css',
    ),
    'inplacewidget_external_js': (
        '//code.jquery.com/jquery-1.9.1.min.js',
        '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js',

    ),

}

TINYMCE_DEFAULT_CONFIG = {
  'file_browser_callback': 'mce_filebrowser',
  'toolbar':"undo redo | styleselect | bold italic | link image",
  'resize':'true',
  'height': 600,
  'width': 600
}
