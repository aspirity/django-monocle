# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'urbantribe.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            _(u'Управление блоками сайта'),
            collapsible=True,
            column=2,
            css_classes=('grp-collapse grp-open',),
            models=('apps.main.models.Section',),
        ))

        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            _(u'Контактная информация'),
            collapsible=True,
            column=1,
            css_classes=('grp-collapse grp-open',),
            models=(
                'apps.main.models.Contacts',
                'apps.monocle_map.*',
            ),
        ))

        self.children.append(modules.ModelList(
            title='Все установленные модули',
            models=('apps.monocle_*',),
            column=1,
        ))


        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Файлы на сервере'),
            column=2,
            children=[
                {
                    'title': _('FileBrowser'),
                    'url': '/admin/filebrowser/browse/',
                    'external': False,
                    'description': "Файлы на сервере",
                },
            ]
        ))

        self.children.append(modules.ModelList(
            title='Пользователи',
            models=('django.contrib.*',),
            column=3,
        ))