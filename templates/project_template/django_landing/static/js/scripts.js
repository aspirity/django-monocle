$(document).ready(function(){


    //menus switch
    $(window).scroll(function () {
        if ($(this).scrollTop() > 130) {
            $('#main-menu').hide();
            $('#fixed-menu').slideDown();
        } else {
            $('#fixed-menu').slideUp(100);
            $('#main-menu').show();
        }
    });


    //smooth scrolling
    $('#main-menu a, #fixed-menu a, a#toadvantages, a#toheader').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top - 40
        }, 500);
        return false;
    });

    //Аллергены раскладка
    $('#show-all-allergens').click(function(){
        if(!$(this).hasClass('active')){
            $('#allergens').css('height','auto');
            $(this).addClass('active');
            $(this).html('Cвернуть')
        } else {

            $('#allergens').css('height','740px');
            $(this).html('Посмотреть все<br/><i style="color: #c0c0c0;" class="fa fa-caret-down fa-lg"></i>');
            $(this).removeClass('active');
        }

    });


    //Карусели

    $('#doctors-car-wrapper').owlCarousel({
        items:4,
        navigation: true,
        navigationText: [
            "<i class='fa fa-angle-left fa-5x'></i>",
            "<i class='fa fa-angle-right fa-5x'></i>"
        ],
        pagination: true,
        responsive: false
    });

    $('#desease-block-wrapper').owlCarousel({
        singleItem:true,
        navigation: true,
        navigationText: [
            "<i class='fa fa-angle-left fa-5x'></i>",
            "<i class='fa fa-angle-right fa-5x'></i>"
        ],
        pagination: false
    });

    $('#therapies-wrap').owlCarousel({
        singleItem:true,
        pagination: true,
        paginationNumbers:true,
    });

    $('#photo-wrap').owlCarousel({
        items: 4,
        navigation:true,
        navigationText: [
            "<i class='fa fa-angle-left fa-5x'></i>",
            "<i class='fa fa-angle-right fa-5x'></i>"
        ],
        pagination: false,
        responsive: false
    });

    $('#partners-wrap').owlCarousel({
        items: 4,
        navigation:true,
        navigationText: [
            "<i class='fa fa-angle-left fa-5x'></i>",
            "<i class='fa fa-angle-right fa-5x'></i>"
        ],
        pagination: false,
        responsive: false
    });

    $('#video-wrap').owlCarousel({
        items: 3,
        navigation:true,
        navigationText: [
            "<i class='fa fa-angle-left fa-5x'></i>",
            "<i class='fa fa-angle-right fa-5x'></i>"
        ],
        pagination: false,
        responsive: false
    });


    //lightbox
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    //parnters switch

    $('.a_parnter').hover(
        function(){
            $(this).find('.gray').hide();
            $(this).find('.colored').show();
        },

        function(){
            $(this).find('.gray').show();
            $(this).find('.colored').hide();
        }
    );

    //modals close button dirty hack

    $('*').click(function(){
        $('button.close').html('');
        $('.modal-header').show();
    });


    var myMap;

    // Дождёмся загрузки API и готовности DOM.
    ymaps.ready(init);

    function init() {
        myMap = new ymaps.Map("ya_maps", {
            center: [55.9835,92.8602],
            zoom: 16,
            controls: []
        });

        myPlacemark = new ymaps.Placemark([55.9835,92.8602], {},
            {
                iconLayout: 'default#image',
                iconImageHref: 'static/imgs/pin.png',
                iconImageSize: [115, 115],

            }
        );
        myMap.geoObjects.add(myPlacemark);

        $('body').on('click', '.close', function (e) {
            e.preventDefault();
            $('.close').parent('div').hide(); myMap.geoObjects.add(myPlacemark);
        });
    }
});