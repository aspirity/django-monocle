[
{
 "model": "{{module_name}}.SampleModel",
 "fields": {
  "name": "Имя1",
  "image": "{{module_name}}/test.jpg",
  "text": "Текст1",
  "isShown": true,
  "position": 0
 },
 "pk": 1
},
{
 "model": "{{module_name}}.SampleModel",
 "fields": {
  "name": "Имя2",
  "image": "{{module_name}}/test.jpg",
  "text": "Текст2",
  "isShown": true,
  "position": 1
 },
 "pk": 2
}
]