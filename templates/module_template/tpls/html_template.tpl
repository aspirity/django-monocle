<section>
    <div class='row'>
        <h2><span>Список объектов</span></h2>
    </div>
    {{'{%'}} for model in {{module_name}}_models {{'%}'}}
    {% raw %}
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-3">
                    <div class="round-border-helper"></div>
                    <img class="img-circle  img-responsive" src="{{ model.image.url }}"/>
                </div>
                <div class='col-xs-9'>
                    <p>
                        {{ model.name }}
                    </p>
                    <p>
                        {{ model.text }}
                    </p>
                </div>
            </div>
        </div>
    {% endfor %}
    {% endraw %}
</section>
