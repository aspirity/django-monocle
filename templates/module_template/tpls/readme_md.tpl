### *{{module_name}} app*
### Модуль: {{module_name}}
====================================================


## **Пример отображения на странице сайта:**
![{{module_name}}](/readme_images/{{module_name}}.png)

## **Пример отображения в панели администрирования:**
![{{module_name}}](/readme_images/{{module_name}}_admin.png)

## **Файл models.py:**
