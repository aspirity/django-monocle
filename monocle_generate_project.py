from config import *
import os
import subprocess

print("\n\n*** Activating Virtualenv ***")
venv_name = os.path.join(PROJECT_PATH, PROJECT_NAME + '_venv')

os.system('virtualenv --no-site-packages -p python3 ' + venv_name)

#continue to process under virtualenv
subprocess.Popen([
    os.path.join(venv_name, 'bin/python3'),
    './monocle_builder.py',
], stderr=subprocess.STDOUT)

print("\n\n*** Сongratulations! Virtualenv was installed and running now ***")