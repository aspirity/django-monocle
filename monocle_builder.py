from config import *
import os, sys, shutil

#installing pip
# os.system(os.path.join(os.path.dirname(os.path.abspath(__file__)), "get-pip.py"))

# install requirements
from package_handler import install_reqs

print("\n\n*** Install Project Requirements ***")
install_reqs(FILE_WITH_REQUIREMENTS)

print("\n\n*** Generate Templates ***")
import django
from package_handler import PackageHandler

from django.core.management import call_command

# Capture our current directory
THIS_DIR = os.path.dirname(os.path.abspath(__file__))

#preparing code generation stuff
import jinja2
from jinja2 import Template, Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.join(THIS_DIR, 'templates/project_template/tpls')))

#get list of requirements
module_list_data = [PackageHandler(module).get_monocle_data() for module in MODULE_LIST]

#delete duplicates
module_list_data = [e for i, e in enumerate(module_list_data) if e not in module_list_data[:i]]

module_list_data_reqs = []

for app in module_list_data:
    print(app)
    for req in app['requirements']:
        module_list_data_reqs.append(req)
module_list_data_reqs = [e for i, e in enumerate(module_list_data_reqs) if e not in module_list_data_reqs[:i]]


context = {
    'PROJECT_PATH': PROJECT_PATH,
    'PROJECT_NAME': PROJECT_NAME,
    'MODULE_LIST': module_list_data,
    'MODULE_LIST_REQS': module_list_data_reqs,
}

#this is the path of the included directory
FULL_PROJECT_PATH = os.path.join(PROJECT_PATH, PROJECT_NAME)

from monocle_functions import *

#let the scaffolding begin
print("\n1. copy_template")
copy_template(THIS_DIR, FULL_PROJECT_PATH)

print("\n2. generate_main_module")
generate_main_module(FULL_PROJECT_PATH, PROJECT_NAME, context, env)

print("\n3. generate_mainapp_view")
generate_mainapp_view(FULL_PROJECT_PATH, context, env)

print("\n4. generate_base_template")
generate_base_template(FULL_PROJECT_PATH, context, env)

print("\n5. generate_manage_py")
generate_manage_py(FULL_PROJECT_PATH, context, env)

print("\n6. generate_main_models")
generate_main_models(FULL_PROJECT_PATH, context, env)

print("\n6. generate_main_admin")
generate_main_admin(FULL_PROJECT_PATH, context, env)

print("\n7. make dir uploads")
upload_path = os.path.join(FULL_PROJECT_PATH, 'public/media', 'uploads')
if not os.path.exists(upload_path):
    os.makedirs(upload_path)

print("\n8. Copy files")
from distutils.sysconfig import get_python_lib

print(FULL_PROJECT_PATH)
print(PROJECT_NAME)

for m_name in [app['appname'] for app in module_list_data]:
    print("\n-- Copy modules to project folder for " + m_name)
    copy(os.path.join(get_python_lib(), m_name), os.path.join(FULL_PROJECT_PATH, 'apps', m_name))

    print("\n-- Copy templates for " + m_name)
    copy(os.path.join(get_python_lib(), m_name, 'templates', m_name), os.path.join(FULL_PROJECT_PATH, 'templates', m_name))

    print("\n-- Copy media files for " + m_name)
    copy(os.path.join(get_python_lib(), m_name, 'fixtures', m_name), os.path.join(FULL_PROJECT_PATH, 'public/media', m_name))

print("\n\n*** Django Specific Commands ***")
#after generation we run django specific commands
sys.path.append(FULL_PROJECT_PATH)
sys.path.append(os.path.join(FULL_PROJECT_PATH, PROJECT_NAME))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", PROJECT_NAME + ".settings")
import django
django.setup()

print("\n1. Migrations")
try:
    call_command('makemigrations', interactive=False)
    call_command('migrate', interactive=False)
    print("Success! Migrations is done")
except:
    print("Error! Migrations wasn't created")


os.chdir(FULL_PROJECT_PATH)
print("\n2. Collect static")
try:
    call_command('collectstatic', interactive=False)
    print("Static was collected")
except:
    print("Error! Static wasn't collected")


print("\n3. Create superuser")
from django.contrib.auth.models import User
try:
    User.objects.create_superuser(username=SUPERUSER_LOGIN, password=SUPERUSER_PASSWORD, email=SUPERUSER_EMAIL)
    print("Success! User " + SUPERUSER_LOGIN + " was created")
except:
    print("Error! User wasn't created")
print("\n4. Generate test data")
for m_name in [app['appname'] for app in module_list_data]:
    print(m_name)
    try:
        print("Test data for module: " + m_name)
        call_command('loaddata', m_name + '.json')
    except:
        print("Module " + m_name + " hasn't test data")

print("\n\n*** Сongratulations! Project was created. ***\n\n")