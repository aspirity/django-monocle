import os, shutil, errno

# Functions

def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

def save_file(filepath, filename, template, FULL_PROJECT_PATH, context, env):
    filepath = os.path.join(FULL_PROJECT_PATH, filepath)

    if not os.path.exists(filepath):
        os.makedirs(filepath)

    f = open(os.path.join(filepath, filename), 'w')

    template = env.get_template(template)
    rendered = template.render(context)

    f.write(rendered)
    f.close()

def make_init(filepath):
    #just for making clear init file
    if not os.path.exists(filepath):
        os.makedirs(filepath)

    f = open(filepath + '/__init__.py','w')
    f.close()

def copy_template(THIS_DIR, FULL_PROJECT_PATH):
    path = THIS_DIR + '/templates/project_template/django_landing'

    if(os.path.exists(FULL_PROJECT_PATH)):
        shutil.rmtree(FULL_PROJECT_PATH)

    shutil.copytree(path,FULL_PROJECT_PATH)

def generate_settings(PROJECT_NAME, FULL_PROJECT_PATH, context, env):
    save_file(PROJECT_NAME,'settings.py','settings.tpl', FULL_PROJECT_PATH, context, env)

def generate_urls(PROJECT_NAME, FULL_PROJECT_PATH, context, env):
    save_file(PROJECT_NAME,'urls.py','urls.tpl', FULL_PROJECT_PATH, context, env)

def generate_wsgi(PROJECT_NAME, FULL_PROJECT_PATH, context, env):
    save_file(PROJECT_NAME,'wsgi.py','wsgi.tpl', FULL_PROJECT_PATH, context, env)

def generate_main_module(FULL_PROJECT_PATH, PROJECT_NAME, context, env):
    #this function start main django module
    generate_settings(PROJECT_NAME, FULL_PROJECT_PATH, context, env)
    generate_urls(PROJECT_NAME, FULL_PROJECT_PATH, context, env)
    generate_wsgi(PROJECT_NAME, FULL_PROJECT_PATH, context, env)
    make_init(os.path.join(FULL_PROJECT_PATH, PROJECT_NAME))

def generate_mainapp_view(FULL_PROJECT_PATH, context, env):
    save_file('apps/main/', 'views.py', 'main/main_view.tpl', FULL_PROJECT_PATH, context, env)

def generate_base_template(FULL_PROJECT_PATH, context, env):
    save_file('templates', 'base.html', 'main/base_template.tpl', FULL_PROJECT_PATH, context, env)

def generate_manage_py(FULL_PROJECT_PATH, context, env):
    save_file('', 'manage.py', 'manage.tpl', FULL_PROJECT_PATH, context, env)

def generate_main_models(FULL_PROJECT_PATH, context, env):
    save_file('apps/main/', 'models.py', 'main/main_models.tpl', FULL_PROJECT_PATH, context, env)

def generate_main_admin(FULL_PROJECT_PATH, context, env):
    save_file('apps/main/', 'admin.py', 'main/main_admin.tpl', FULL_PROJECT_PATH, context, env)

# /Functions