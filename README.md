# **Содержание:**

### 1. *[Сборщик Django-monocle](#markdown-header-django-monocle)*
### 2. *[Модуль для проекта, созданного с помощью системы Monocle](#markdown-header-monocle)*

---
# **Сборщик Django-monocle**
---

## **О сборщике:**

* Данная система создана для быстрой сборки проектов одностраничных сайтов на django из доступных модулей (каждый из которых размещено в своем репозитории). Список модулей можно посмотреть по ссылке: https://bitbucket.org/langprism-monoculus
* После заполнения конфигурационного файла и списка необходимых пакетов (модулей) пользователем, сборщик формирует готовый пакет содержащий нужные модули.
* Сборщик позволяет генерировать модули для последующей их доработки

## **Важная информация:**

* Проект предназначен для работы на языке Python 3.X и выше.
* Перед запуском процесса сборки нужно убедиться, что установлен Virtualenv

Для начала работы со сборщиком необходимо выполнить:

    sudo apt-get install python3-dev zlib1g-dev libfreetype6 libfreetype6-dev libjpeg-dev

Если Virtualenv не установлен, ничего страшного, просто выполните в терминале команду:

    sudo apt-get install python-virtualenv

* Сборщик создает свое виртуальное окружение для каждого создаваемого проекта
* Сборшик копирует папки с установленными приложениями в директорию apps созданного проекта
* Папки с шаблонами приложений копируются в директорию templates созданного проекта
* Если возникает проблема с PIL (Pillow, zlib, libjpg, libpng), нужно прочитать инструкции: 

    http://vivazzi.ru/blog/10/

    http://sorl-thumbnail.readthedocs.org/en/latest/requirements.html

Если все-равно возникает проблема с декодером jpeg, то нужно выполнить:

    ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so ~/venv/lib/
    ln -s /usr/lib/x86_64-linux-gnu/libfreetype.so.6 ~/venv/lib/
    ln -s /usr/lib/x86_64-linux-gnu/libz.so ~/venv/lib/

~/venv/lib/ нужно заменить на путь до вашего виртуального окружения.

Затем нужно переустановить Pillow (при активированном виртуальном окружении):
    
    pip3 install -I Pillow

## **Содержание документации для сборщика**

* _*Структура сборщика*_
* _*Порядок работы со сборщиком проекта*_
* _*Порядок работы со сборщиком модулей*_


### **Структура сборщика**

Сборщик проектов и модулей имеет следующую структуру:

* teplates - дирректория, в которой хранится шаблоны для генерации проекта и модулей
* teplates/project_template/tpls, teplates/module_template/tpls - папка с шаблонами Jinja2 используемые для генерации основных файлов нового проекта или модуля для одностраничного сайта
* config.py - файл, в котором прописываются основные настройки для сборки
* monocle_generate_project.py - файл сборщика, запускающий процесс генерации нового проекта
* monocle_generate_module.py - файл сборщика, запускающий процесс генерации нового модуля
* package_handler.py - модуль отвественный за загрузку пакетов и предоставление данных для интеграции
* monocle_functions.py - файл с функциями для сборки проекта
* reqs.txt - файл с зависимостями, которые нужно установить и подключить (помимо зависимостей, указанных в этом файле, также устанавливаются зависимости для каждого модуля в отдельности)
* README.md - Документация для сборщика проектов и модулей
* *https://bitbucket.org/langprisminternal/django-monocle - репозиторий, содержащий текущий сборщик*
* *https://bitbucket.org/langprism-monoculus - репозиторий с модулями*


### **Порядок работы со сборщиком проекта**

*1) Склонировать сборщик с репозитория https://bitbucket.org/langprisminternal/django-monocle*

*2) Заполнить конфигурационный файл (config.py):*

    #родительская директория,в которую будет загружаться проект
    PROJECT_PATH = '/home/user/'

    #название проекта
    PROJECT_NAME = 'demoproject'

    #cписок модулей используемых в проекте находиться здесь - https://bitbucket.org/monoculus
    MODULE_LIST = [
       'monocle_package1',
       'monocle_package2',
    ]

    #
    SUPERUSER_LOGIN = 'admin'
    SUPERUSER_PASSWORD = 'admin'
    SUPERUSER_EMAIL = 'admin@admin.ru'

    FILE_WITH_REQUIREMENTS - файл с зависимостями проекта

*3) Указать зависимости для проекта в файле заданном в переменной FILE_WITH_REQUIREMENTS*

*Пример:*

    easy_thumbnails
    django-summernote

*4) Запустить файл monocle_generate_project.py:*

    python3 monocle_generate_project.py
    или
    sh generate_project.sh

![Run generator](/readme_images/generate.png)

*5) Немного подождать, пока сборщик собирает проект*

*6) Все! Виртуальное окружение Python и проект успешно созданы*

![Project was created](/readme_images/finish.png)

Для запуска проекта необходимо активировать созданное виртуальное окружение Python (из его директории. Например: tesproject3_venv):

    source ./bin/activate

и из папки с проектом запустить тестовый сервер Django:
    
    python3 manage.py runserver

Изначально страница сайта пуста. Блоки на нее можно добавить через панель администрирования.

### **Порядок работы со сборщиком модулей**

*1) Склонировать сборщик с репозитория https://bitbucket.org/langprisminternal/django-monocle*

*2) Запустить файл monocle_generate_module.py:*

    python3 monocle_generate_module.py НАЗВАНИЕ_МОДУЛЯ ПУТЬ_К_МОДУЛЮ

+ НАЗВАНИЕ_МОДУЛЯ - название создаваемого модуля (в названии следует использовать префикс "monocle_").
+ ПУТЬ_К_МОДУЛЮ - путь к этому модулю

![Run module generator](/readme_images/generate_module.png)

*3) Модуль успешно создан! Теперь можно перейти к его доработке.*

*Обратите внимание:*

 > Если не передать никаких аргументов, то в качестве названия модуля будет использоваться "test", а путем к модулю станет папка родительская к текущей (../).
 > Если будет передан один аргумент, то он станет названием нового модуля, а путь останется (../)

Подробнее прочитать о разработке модуля для проекта и посмотреть образцы кода можно по ссылке - https://bitbucket.org/langprisminternal/monocle_sample


---
# **Модуль для проекта, созданного с помощью системы Monocle**
---

Это демо-модуль созданный в качестве примера для разработки других модулей для сборщика одностраничных сайтов django-monocle.
Модуль устанавливается сборщиком с помощью менеджера pip3, поэтомоу должен быть собран и упакован с помощью него.

## **Содержание документации для модуля проекта Monocle**

* _*Структура папок модуля*_
* _*Конфигурационный файл*_
* _*json файл импорта*_
* _*Примеры файлов (Модели и Админка)*_
* _*Публикация через pip3*_
* _*Внесение изменений в модули*_


## **Структура папок модуля**

Модуль представляет собой стандартное приложение для django и включает в себя модели, средства генерации систем управления, шаблоны, статические файлы, тестовые данные (файлы импорта), список зависимостей, а также файлы для интеграции и сборки.
Так как модуль встраивается в одностраничный сайт с помощью сборщика, то у него нет файлов view.py и urls.py. Для передачи данных из модуля в основной проект используется файл monocle.py, содеражащий нужные вызовы для определенного модуля.
Модули являются частями проекта автоматической сборки одностраничных сайтов на django - https://bitbucket.org/langprisminternal/django-monocle.

*Необходимо соблюдать следующие условия наименования файлов и папок:*

    * monocle_{{PROJECT_NAME}}
        * templates
            * monocle_{{PROJECT_NAME}}
                *monocle_{{PROJECT_NAME}}.html
        * static
            * monocle_{{PROJECT_NAME}}
                * assets
                * monocle_{{PROJECT_NAME}}.css
                * monocle_{{PROJECT_NAME}}.js
        * fixtures
            *monocle_{{PROJECT_NAME}}
                [Картинки]
            *monocle_{{PROJECT_NAME}},json
        * models.py
        * admin.py
        * monocle.py
    * MANIFEST.in
    * README.md
    * images_readme
        [Картинки для документации модуля]
    * setup.py
    * reqs.txt


### **Конфигурационный файл модуля**

Для сборки и интеграции проекта сборщик django-monocle использует файл модуля monocle.py:


    # Название модуля - должно совпадать с {{PROJECT_NAME}}
    appname = 'monocle_sample'

    # Модели импортируемые в основное приложение одностраничного проекта
    models = ['SampleModel']

    """
        строка передающая данные из модели в контекст основного шаблона. Этот вызов используется во view.py файле основого приложения проекта при сборке.
    """
    context_callback =  "'monocle_sample_models': SampleModel.objects.all().filter(isShown=True)"

    # Зависимости, подключаемые в файле settings.py проекта.
    included_app_reqs = [ ]



### **json файл импорта (пример)**

	Пример 1:
    [
    {
     "model": "monocle_partners.partner",
     "fields": {
      "position": 0,
      "image": "monocle_partners/image.jpg",
      "name": "Партнер1",
      "isShown": true
     },
     "pk": 1
    },
    {
     "model": "monocle_partners.partner",
     "fields": {
      "position": 2,
      "image": "monocle_partners/image.jpg",
      "name": "Партнер2",
      "isShown": true
     },
     "pk": 2
    }
    ]

    Пример 2:
    [
    {"pk": 1, "model": "monocle_slider.slider", "fields": {"text": "Описание для карусели", "isShown": true, "elem_number": 3, "arrows": "true", "name": "Карусель", "position": 0, "pagination": "true"}},
    {"pk": 1, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 1", "isShown": true, "image": "monocle_slider/Chrysanthemum.jpg", "slider": 1, "name": "Слайд 1", "position": 0}},
    {"pk": 2, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 2", "isShown": true, "image": "monocle_slider/Desert.jpg", "slider": 1, "name": "Слайд 2", "position": 1}},
    {"pk": 3, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 3", "isShown": true, "image": "monocle_slider/Koala.jpg", "slider": 1, "name": "Слайд 3", "position": 2}},
    {"pk": 4, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 4", "isShown": true, "image": "monocle_slider/Penguins.jpg", "slider": 1, "name": "Слайд 4", "position": 3}}
    ]


### **Примеры файлов (Модели и файл для панели администрирования)**

#### models.py:

	from django.db import models
	from solo.models import SingletonModel

	from django.conf import settings

	CHOICES = [[app.replace("apps.", ''), app.replace("apps.", ''),] for app in settings.MY_APPS]

	class Section(SingletonModel):
		name = models.CharField(max_length=255, default='Секция с блоками сайта', verbose_name= 'Название секции с блоками')

		def __str__(self):
			return self.name
		class Meta:
			verbose_name_plural = u'Блоки'
			verbose_name = u'Блок'

	class Block(models.Model):
		name = models.CharField(max_length=255, blank=True, verbose_name= 'Название блока')
		Section = models.ForeignKey('Section', verbose_name= 'Секция')
		app_name = models.CharField(choices=CHOICES, default='true', max_length=255, verbose_name= 'Название monocle-приложения', help_text='Пример: monocle_main')
		text = models.TextField(verbose_name='Описание', blank=True)
		position = models.SmallIntegerField(default=0)
		isShown = models.BooleanField(default=True, verbose_name='отображать блок')

		def __str__(self):
			return self.name

		class Meta:
			verbose_name_plural = u'Блоки'
			verbose_name = u'Блок'
			ordering = ['position']

	class Contacts(SingletonModel):
		 phone = models.CharField(max_length=100, blank=True, verbose_name='Телефон', help_text='Введите номер телефона')
		 email = models.EmailField(verbose_name='Email для приема заказов', help_text='Введите адрес электронной почты')
		 address = models.TextField(verbose_name='Адрес', help_text='Введите адрес')

		 class Meta:
			 verbose_name = u'Контакты'
			 verbose_name_plural = u'Контакты'

	from django.apps import AppConfig
	class CustomAppConfig(AppConfig):
		name = 'apps.main'
		verbose_name = 'Основные настройки'


#### admin.py:

	from django.contrib import admin
	from .models import *
	from grappelli.forms import GrappelliSortableHiddenMixin

	from django_summernote.admin import SummernoteModelAdmin
	from django_summernote.admin import SummernoteInlineModelAdmin

	class BlockInline(GrappelliSortableHiddenMixin, admin.TabularInline, SummernoteInlineModelAdmin):
		model = Block
		sortable_field_name = "position"
		extra = 0

		fieldsets = [
			('', {'fields': ['app_name', 'name', 'isShown', 'position', ], 'classes': ['collapse']}),
		]
		list_display = ('name', 'app_name', 'text', 'position', 'isShown',)

	class SectionAdmin(SummernoteModelAdmin):
		list_display = ('name',)
		list_editable = ('name',)
		inlines = [BlockInline]

	class ContactsAdmin(SummernoteModelAdmin):
		fieldsets = [
			('Контактная информация', {'fields': ['phone', 'email', 'address'], 'classes': ['collapse']}),
		]
		list_display = ('phone', 'email', 'address')
		list_editable = ('phone', 'email', 'address')

	admin.site.register(Contacts, ContactsAdmin)
	admin.site.register(Section, SectionAdmin)


#### __init.py__:

    default_app_config = 'apps.main.models.CustomAppConfig'


### **Генерация тестовых данных**:

    python3 manage.py  dumpdata APPNAME --format json

Результат будет выведен в консоль. Его можно использовать в дальнейшем для создания файла импорта.


### **Публикация через pip3**

Модули устанавливаются сборщиком при помощи менеджера пакетов pip3. Поэтому после внесения изменений в модуль необходимо собрать его в дистрибутив и опубликовать в PyPi.
Для этого нужно отредактировать файл setup.py:

    setup(
        name='{{PROJECT_NAME}}',
        version='0.1.0',
        packages=['{{PROJECT_NAME}}'],
        include_package_data=True,
        install_requires=[
        "requests",
        "bcrypt",
        ],
        license='BSD License',  # example license
        description='Sample app for django-monocle project',
        long_description=README,
        author='Alexander Kalinin @Langprism LTD',
        author_email='ak@langprism.com',
        classifiers=[
            'Environment :: Web Environment',
            'Framework :: Django',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: BSD License', # example license
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            # Replace these appropriately if you are stuck on Python 2.
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.2',
            'Programming Language :: Python :: 3.3',
            'Topic :: Internet :: WWW/HTTP',
            'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ],
    )

Зависимости пакетов указываются в файле setup.py в атрибуте "install_requires" (см. пример выше).

После редактирования нужно выполнить команду:

    python3 setup.py register sdist bdist_wheel upload
    или
    sh upload.sh

Для публикации дистрибутива в индексе нужно ввести данные аккаунта проекта:
логин - monoculus,
пароль - Langprism11

### **Внесение изменений в модули**

В случае если модуль необходимо доработать либо внести изменения, необходимо склонировать его с репозитория. Хранилище модулей находится по ссылке https://bitbucket.org/langprism-monoculus.
После изменений необходимо снова опубликовать проект pypi, инкрементировать номер версии и выполнить push в репозиторий.