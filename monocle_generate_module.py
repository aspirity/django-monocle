'''
This module is designed for scaffolding of monocle modules.
Run python3 monocle_scaffold.py module_name /path/to/directory for creating new module

Use
import from monocle_scaffold import scaffold
scaffold(module_name, module_path)
to create module from script
'''

print("\n*** Start module generation ***\n")

import sys, os, shutil

import jinja2
from jinja2 import Template
from jinja2 import Environment, FileSystemLoader

#take args from console
module_name = 'test'
module_path = os.path.abspath('..')

args_number = len(sys.argv)
if args_number == 3:
    module_name = sys.argv[1]
    module_path = sys.argv[2]
elif args_number == 2:
    module_name = sys.argv[1]

#preparing code generation stuff
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader= FileSystemLoader(os.path.join(THIS_DIR , 'templates/module_template/tpls')))

class MonocleScaffolder():

    def __init__(self, module_name, module_path):
        self.module_name = module_name
        self.module_path = module_path
        self.this_dir = THIS_DIR

    def make_path(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def render_tpl(self, template, context):
        template =  env.get_template(template)
        rendered = template.render(context)
        return rendered

    def save_file(self, filepath, filename, template):

        self.make_path(filepath)
        context = {
            'module_name': self.module_name
        }
        rendered = self.render_tpl(template, context)

        f = open(os.path.join(filepath, filename), 'w')
        f.write(rendered)
        f.close()


    def scaffold(self):
        module_path = self.module_path
        module_name = self.module_name

        #create dist and module directory
        dist_dir = self.make_path(os.path.join(module_path, module_name))
        module_dir = os.path.join(dist_dir, module_name)

        #copy admin and models
        try:
            shutil.copytree(os.path.join(self.this_dir,'templates/module_template/module_files/monocle_sample'), module_dir)
        except OSError:
            shutil.rmtree(module_dir)
            shutil.copytree(os.path.join(self.this_dir,'templates/module_template/module_files/monocle_sample'), module_dir)

        #copy the script for easy upload module in PyPi
        shutil.copy(os.path.join(self.this_dir, 'templates/module_template/module_files/upload.sh'), module_dir)


        #populate monocle_py
        print("\n1. Сreate monocle.py")
        self.save_file(module_dir, 'monocle.py', 'monocle_py.tpl')

        print("\n2. Сreate setup.py")
        self.save_file(dist_dir, 'setup.py', 'setup_py.tpl')

        print("\n3. Сreate MANIFEST.in")
        self.save_file(dist_dir, 'MANIFEST.in', 'manifest_in.tpl')

        print("\n3. Сreate README.rst")
        self.save_file(dist_dir, 'README.md', 'readme_md.tpl')

        print("\n4. Сreate templates")
        templates_dir = self.make_path(os.path.join(module_dir,'templates/' + module_name))

        #populate templates directory
        self.save_file(templates_dir, self.module_name + '.html', 'html_template.tpl')

        print("\n5. Сreate static")
        #create static directory
        templates_dir = self.make_path(os.path.join(module_dir,'static/' + module_name))

        open(os.path.join(templates_dir, module_name + '.css'), 'tw', encoding='utf-8')
        open(os.path.join(templates_dir, module_name + '.js'), 'tw', encoding='utf-8')

        print("\n6. Сreate fixtures")
        #create fixtures directory
        self.make_path(os.path.join(module_dir, 'fixtures', module_name))
        fixtures_dir = os.path.join(module_dir, 'fixtures')

        #populate fixtures directory
        self.save_file(fixtures_dir, self.module_name + '.json', 'fixtures_py.tpl')

module = MonocleScaffolder(module_name, module_path)
module.scaffold()

print("\n\n*** Сongratulations! Module was created ***")

