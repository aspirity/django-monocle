# project config

# this is the path of the parent directory
PROJECT_PATH = '/home/' #change this path

PROJECT_NAME = 'testproject3'

MODULE_LIST = [
    'monocle_menu',
    'monocle_mainbanner',
    'monocle_timer',
    'monocle_reviews',
    'monocle_slider',
    'monocle_partners',
    'monocle_social_buttons',
    'monocle_map',
    'monocle_modals',
   # 'monocle_youtube',
]

SUPERUSER_LOGIN = 'admin'
SUPERUSER_PASSWORD = 'admin'
SUPERUSER_EMAIL = 'admin@admin.ru'

# Monocle package manager config
FILE_WITH_REQUIREMENTS = 'reqs.txt'
