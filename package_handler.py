import pip

class PackageHandler():

    def __init__(self,package_name):
        self.package_name = package_name
        self.install_package()

    def install_package(self):
        pip.main(['install', self.package_name])
        pip.logger.consumers = []

    def get_monocle_data(self):

        package = __import__(self.package_name + '.monocle')

        #for reverse compatibilty if it's absent
        if hasattr(package.monocle, 'included_app_reqs'):
            reqs = package.monocle.included_app_reqs
        else:
            reqs = []

        data = {
            'context_callback': package.monocle.context_callback,
            'models': package.monocle.models,
            'appname': package.monocle.appname,
            'requirements': reqs,
        }
        return data

def install_reqs(FILE_WITH_REQUIREMENTS):
    file_reqs = open(FILE_WITH_REQUIREMENTS, 'r')
    list_reqs = [line.strip() for line in file_reqs]
    for req in list_reqs:
        pip.main(['install', req])
        pip.logger.consumers = []
    file_reqs.close()